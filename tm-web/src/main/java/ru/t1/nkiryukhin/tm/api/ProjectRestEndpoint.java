package ru.t1.nkiryukhin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface ProjectRestEndpoint {

    @Nullable
    Collection<Project> findAll();

    void save(@NotNull Project project);

    @Nullable
    Project findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    long count() throws Exception;

    void deleteById(@NotNull String id);

    void delete(@NotNull Project project);

    void clear(List<Project> projects);

    void clear();

}
