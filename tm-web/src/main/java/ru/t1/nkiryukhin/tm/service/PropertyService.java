package ru.t1.nkiryukhin.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.IDatabaseProperty;


@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IDatabaseProperty {

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.name']}")
    private String databaseName;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSQL;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['database.server.url']}")
    private String databaseServerUrl;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLevelCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheProviderConfigurationFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactoryClass;

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return databaseServerUrl + databaseName;
    }

}