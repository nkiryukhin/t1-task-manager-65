package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.ProjectRestEndpoint;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @PostMapping("/save")
    public void save(@RequestBody final @NotNull Project project) {
        projectRepository.save(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") final @NotNull String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) {
        return projectRepository.findById(id).isPresent();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final Project project) {
        projectRepository.deleteById(project.getId());
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@RequestBody @NotNull List<Project> projects) {
        for (Project project : projects) {
            projectRepository.deleteById(project.getId());
        }
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        projectRepository.deleteAll();
    }

}