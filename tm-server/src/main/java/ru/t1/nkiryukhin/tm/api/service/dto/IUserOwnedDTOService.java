package ru.t1.nkiryukhin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.nkiryukhin.tm.enumerated.CustomSorting;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.Collection;
import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    M add(@Nullable String userId, M model) throws UserIdEmptyException;

    void clear(@NotNull String userId) throws UserIdEmptyException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    @Nullable
    List<M> findAll(@NotNull String userId) throws UserIdEmptyException;

    @Nullable
    List<M> findAll(@NotNull String userId, @Nullable CustomSorting sort) throws UserIdEmptyException;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractFieldException;

    int getSize(@NotNull String userId) throws UserIdEmptyException;

    void remove(@Nullable Collection<M> collection) throws AbstractException;

    void removeById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    void removeOne(@Nullable M model);

}
