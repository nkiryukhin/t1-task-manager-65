package ru.t1.nkiryukhin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws UserIdEmptyException;

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    void updateProjectId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String projectId
    );

}
