package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DatabaseCreateResponse extends AbstractResponse {
}